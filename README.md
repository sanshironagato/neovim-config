# neovim-config
Just my basic neovim configuration for Window :>.
---
## Installation

I use [vim-plug](https://github.com/junegunn/vim-plug) to install plugins because it is easy to use.

There are 2 way to use this config:
1. Just copy what you need :>.
2. Clone the whole repo to your computer and copy all of it to `~Users\yourcomputer_name\AppData\Local\nvim\`. If you don't have `nvim` file, just create it :>.

```
git clone https://github.com/ducnguyen1511/neovim-config
```

When it done, open `init.vim` by `nvim-qt.exe` and then type `:PlugInstall` in `Command` mode, don't forget to `:PlugUpdate` for your plugins. And final, exit your `init.vim` ( don't forget to save it ) and enjoy your new neovim :>. 

## Some screenshots 

-Start screen
![image](https://user-images.githubusercontent.com/47920109/135983840-ff618a64-fc43-475d-878e-511bee49b36e.png)

-Nerdtree + Floating terminal
![image](https://user-images.githubusercontent.com/47920109/135984322-2448003c-43bc-4174-8371-94c6c2708be6.png)
